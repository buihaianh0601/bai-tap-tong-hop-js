function inSo() {
  for (let i = 0; i < 10; i++) {
    let daySo = "";
    for (let j = 1; j <= 10; j++) {
      daySo += (j + i * 10).toString() + " ";
    }
    $("#mangSo").append("<div>" + daySo + "</div>");
  }
}
var mangSoNguyen = [];
function themSo() {
  soThemVao = document.getElementById("numberToAdd").value * 1;
  mangSoNguyen.push(soThemVao);
  $("#mangSoNguyen").append(document.getElementById("numberToAdd").value + " ");
  soThemVao = document.getElementById("numberToAdd").value = "";
}

function inSoNT() {
  let mangSoNguyenLength = mangSoNguyen.length;
  let daySoNT = "";
  for (let i = 1; i <= mangSoNguyenLength; i++) {
    let demSoNT = 0;
    for (let j = 1; j <= mangSoNguyen[i]; j++) {
      if (mangSoNguyen[i] % j == 0) {
        demSoNT++;
      }
    }
    if (demSoNT == 2) {
      daySoNT += mangSoNguyen[i].toString() + " ";
    }
  }
  $("#dsSoNT").append("<div>" + daySoNT + "</div>");
}

function bai3TinhTong() {
  let n = document.getElementById("soNBai3").value * 1;
  let bai3TongTemp = 0;
  for (let i = 2; i <= n; i++) {
    bai3TongTemp += i;
  }
  bai3TongTemp += 2 * n;
  $("#bai3Tong").append("<div>" + bai3TongTemp + "</div>");
}

function bai4Function() {
  let bai4OutputJS = "";
  let n = document.getElementById("bai4Input").value * 1;
  for (let i = n; i >= 1; i--) {
    if (n % i == 0) {
      bai4OutputJS += i.toString() + " ";
    }
  }
  $("#bai4Output").append("<div>" + bai4OutputJS + "</div>");
}

function bai5() {
  let rev = 0;
  let num = document.getElementById("bai5Input").value * 1;
  let lasDigit;
  while (num != 0) {
    lasDigit = num % 10;
    rev = rev * 10 + lasDigit;
    num = Math.floor(num / 10);
  }
  $("#bai5Output").append("<div>" + rev + "</div>");
}

function bai6() {
  let i=0;
  let sum = 0;
  while(sum<100){
    i++;
    sum+=i;
  }
  let j=i-1;
  $("#bai6Output").append("<div>" + j + "</div>");
}

function bai7(){
  bai7InputTemp=document.getElementById("bai7Input").value*1;
  let i=0;
  while(i<=10){
    $("#bai7Output").append("<div>" + bai7InputTemp + " x "+ i +" = "+ i*bai7InputTemp +"</div>");
    i++;
  }
}

function bai8(){
  let cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H","10S", "AS", "7H", "9K", "10D"];
  let player1=[],player2=[],player3=[],player4=[];
  for(let i=0;i<=9;i+=4){
    player1.push(cards[i]);
    player2.push(cards[i+1]);
    player3.push(cards[i+2]);
    player4.push(cards[i+3]);
  }
  $("#bai8Output").append("<div> player1 =["+ player1 +"]</div>");
  $("#bai8Output").append("<div> player2 =["+ player2 +"]</div>");
  $("#bai8Output").append("<div> player3 =["+ player3 +"]</div>");
  $("#bai8Output").append("<div> player4 =["+ player4 +"]</div>");
}

function bai9(){
  let tongSoConE=document.getElementById("bai9InputTongSoCon").value*1, tongSoChanE=document.getElementById("bai9InputTongSoChan").value*1;
  let ga=(tongSoChanE-4*tongSoConE)/2;
  let cho=tongSoConE-ga;
  $("#bai9Output").append("<div> Số chó: "+ cho +"</div>");
  $("#bai9Output").append("<div> Số gà: "+ ga +"</div>");
}

function bai10(){
  let soGioE=document.getElementById("soGioInput").value*1, soPhutE=document.getElementById("soPhutInput").value*1;
  let goc=Math.abs(6*soPhutE-0.5*(soGioE*60+soPhutE));
  $("#bai10Output").append("<div> Góc tạo bởi kim giờ & phút: "+ goc.toString() +" độ</div>");
}